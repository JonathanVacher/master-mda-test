---
title: Équipe
layout: teams
description: Équipe
permalink: "/equipe/"
intro_image_absolute: true
intro_image_hide_on_mobile: false
---

# Équipe enseignante

Notre équipe est composée d'enseignants-chercheurs et d'enseignantes-chercheuses du laboratoire [MAP5](https://map5.mi.parisdescartes.fr/) et d'ailleurs.

