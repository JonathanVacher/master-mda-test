---
title: Master MDA
layout: home
description: Master en mathématiques appliquée d'Université Paris Cité.
intro_image: "images/illustrations/pointing.svg"
intro_image_absolute: true
intro_image_hide_on_mobile: true
show_call_box: true
---

# Master MDA - Mathématiques Données Apprentissage

La mention de master Mathématiques, Données, Apprentissage comporte trois parcours en M2 : 
- Ingénierie Mathématique et Biostatistique (IMB) 
- Mathématiques Modélisation Apprentissage (MMA) 
- Mathématiques, Vision, Apprentissage, en partenariat avec l’Université Paris Saclay et l’ENS Paris Saclay

--------------------------------------------------
